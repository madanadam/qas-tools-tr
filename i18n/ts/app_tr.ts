<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="tr_TR">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>ALSA::CTL_Arg_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="116"/>
        <source>CARD</source>
        <translation>Kart</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="117"/>
        <source>SOCKET</source>
        <translation>Soket</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="118"/>
        <source>CTL</source>
        <translation>CTL</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_IFace_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="96"/>
        <source>CARD</source>
        <translation>Kart</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="97"/>
        <source>HWDEP</source>
        <translation>Özellikler</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="98"/>
        <source>MIXER</source>
        <translation>Karıştırıcı</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="99"/>
        <source>PCM</source>
        <translation>PCM</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="100"/>
        <source>RAWMIDI</source>
        <translation>MIDI</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="101"/>
        <source>TIMER</source>
        <translation>Zamanlayıcı</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="102"/>
        <source>SEQUENCER</source>
        <translation>Sıralayıcı</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="103"/>
        <location filename="../../shared/src/qsnd/mixer_hctl_info_db.cpp" line="95"/>
        <source>Unknown</source>
        <translation>Bilinmeyen</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_Type_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="106"/>
        <source>NONE</source>
        <translation>YOK</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="107"/>
        <source>BOOLEAN</source>
        <translation>Mantıksal</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="108"/>
        <source>INTEGER</source>
        <translation>Tamsayı</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="109"/>
        <source>ENUMERATED</source>
        <translation>Numaralı</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="110"/>
        <source>BYTES</source>
        <translation>Bayt</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="111"/>
        <source>IEC958</source>
        <translation>IEC958</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="112"/>
        <source>INTEGER64</source>
        <translation>Integer64</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="113"/>
        <location filename="../../shared/src/qsnd/mixer_hctl_info_db.cpp" line="59"/>
        <source>Unknown</source>
        <translation>Bilinmeyen</translation>
    </message>
</context>
<context>
    <name>ALSA::Channel_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="85"/>
        <source>Front Left</source>
        <translation>Ön Sol</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="86"/>
        <source>Front Right</source>
        <translation>Ön Sağ</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="87"/>
        <source>Rear Left</source>
        <translation>Arka Sol</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="88"/>
        <source>Rear Right</source>
        <translation>Arka Sağ</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="89"/>
        <source>Front Center</source>
        <translation>Ön Orta</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="90"/>
        <source>Woofer</source>
        <translation>Woofer</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="91"/>
        <source>Side Left</source>
        <translation>Yan Sol</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="92"/>
        <source>Side Right</source>
        <translation>Yan Sağ</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="93"/>
        <source>Rear Center</source>
        <translation>Arka Orta</translation>
    </message>
</context>
<context>
    <name>ALSA::Elem_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="47"/>
        <source>Master</source>
        <translation>Ana</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="55"/>
        <source>Speaker</source>
        <translation>Hoparlör</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="41"/>
        <source>Headphone</source>
        <translation>Kulaklık</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="42"/>
        <source>Headphone LFE</source>
        <translation>LFE Kulaklık</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="43"/>
        <source>Headphone Center</source>
        <translation>Kulaklık Orta</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="38"/>
        <source>Front</source>
        <translation>Ön</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="29"/>
        <source>Auto-Mute Mode</source>
        <translation>Oto-Ses-Kıs Modu</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="39"/>
        <source>Front Mic</source>
        <translation>Ön Mikrofon</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="40"/>
        <source>Front Mic Boost</source>
        <translation>Ön Mik. Güçlendirme</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="48"/>
        <source>Mic</source>
        <translation>Mikrofon</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="49"/>
        <source>Mic Boost</source>
        <translation>Mik. Güçlendirme</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="53"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="56"/>
        <source>Surround</source>
        <translation>Ses Sistemi</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="58"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="33"/>
        <source>Center</source>
        <translation>Orta</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="54"/>
        <source>Side</source>
        <translation>Yan</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="32"/>
        <source>Capture</source>
        <translation>Yakala</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="31"/>
        <source>Beep</source>
        <translation>Bip</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="52"/>
        <source>PC Speaker</source>
        <translation>PC Hoparlörü</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="30"/>
        <source>Bass</source>
        <translation>Bas</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="57"/>
        <source>Treble</source>
        <translation>Tiz</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="34"/>
        <source>Channel Mode</source>
        <translation>Kanal Modu</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="28"/>
        <source>Auto Gain Control</source>
        <translation>Oto kazanç kontrolü</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="51"/>
        <source>Mic Select</source>
        <translation>Mikrofon Seç</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="45"/>
        <source>Internal Mic</source>
        <translation>Dahili Mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="46"/>
        <source>Int Mic</source>
        <translation>İç Mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="36"/>
        <source>External Mic</source>
        <translation>Harici Mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="37"/>
        <source>Ext Mic</source>
        <translation>Dış Mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="50"/>
        <source>Mic Boost (+20dB)</source>
        <translation>Mik. Güçlendirme (+20dB)</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="35"/>
        <source>External Amplifier</source>
        <translation>Harici Amplifikatör</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="44"/>
        <source>Input Source</source>
        <translation>Giriş Kaynağı</translation>
    </message>
</context>
<context>
    <name>ALSA::Enum_Value</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="64"/>
        <source>1ch</source>
        <translation>1 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="65"/>
        <source>2ch</source>
        <translation>2 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="66"/>
        <source>3ch</source>
        <translation>3 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="67"/>
        <source>4ch</source>
        <translation>4 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="68"/>
        <source>5ch</source>
        <translation>5 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="69"/>
        <source>6ch</source>
        <translation>6 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="70"/>
        <source>7ch</source>
        <translation>7 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="71"/>
        <source>8ch</source>
        <translation>8 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="74"/>
        <source>Internal Mic</source>
        <translation>Dahili Mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="75"/>
        <source>Mic</source>
        <translation>Mikrofon</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="73"/>
        <source>Front Mic</source>
        <translation>Ön Mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="61"/>
        <source>Enabled</source>
        <translation>Etkin</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="62"/>
        <source>Disabled</source>
        <translation>Devredışı</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="76"/>
        <source>Mic1</source>
        <translation>Mik. 1</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="77"/>
        <source>Mic2</source>
        <translation>Mik. 2</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="78"/>
        <source>Mic3</source>
        <translation>Mik. 3</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="79"/>
        <source>Mic4</source>
        <translation>Mik. 4</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="81"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="82"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
</context>
<context>
    <name>ALSA::Flags</name>
    <message>
        <location filename="../../shared/src/qsnd/mixer_hctl_info_db.cpp" line="101"/>
        <source>not readable</source>
        <translation>okunamaz</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/mixer_hctl_info_db.cpp" line="103"/>
        <source>readable</source>
        <translation>okunabilir</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/mixer_hctl_info_db.cpp" line="109"/>
        <source>not writable</source>
        <translation>yazılamaz</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/mixer_hctl_info_db.cpp" line="111"/>
        <source>writable</source>
        <translation>yazılabilir</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/mixer_hctl_info_db.cpp" line="117"/>
        <source>not volatile</source>
        <translation>değiştirilemez</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/mixer_hctl_info_db.cpp" line="119"/>
        <source>volatile</source>
        <translation>değiştirilebilir</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/mixer_hctl_info_db.cpp" line="125"/>
        <source>not active</source>
        <translation>etkin değil</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/mixer_hctl_info_db.cpp" line="127"/>
        <source>active</source>
        <translation>etkin</translation>
    </message>
</context>
<context>
    <name>MWdg::Inputs_Setup</name>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="35"/>
        <source>s</source>
        <extracomment>&quot;s&quot; is an abbreviation for &quot;split&quot; or &quot;separate&quot;. Something like &quot;j&quot; for &quot;joined&quot; or &quot;c&quot; for &quot;channel&quot; may be appropriate, too.</extracomment>
        <translatorcomment>Trennen</translatorcomment>
        <translation>t</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="39"/>
        <source>l</source>
        <extracomment>&quot;l&quot; is an abbreviation for &quot;level&quot;</extracomment>
        <translatorcomment>Nivellieren</translatorcomment>
        <translation>n</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="43"/>
        <source>m</source>
        <extracomment>&quot;m&quot; is an abbreviation for &quot;mute&quot;</extracomment>
        <translatorcomment>Stumm</translatorcomment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="47"/>
        <source>p</source>
        <extracomment>&quot;p&quot; is an abbreviation for &quot;playback&quot;</extracomment>
        <translatorcomment>Wiedergabe</translatorcomment>
        <translation>w</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="51"/>
        <source>c</source>
        <extracomment>&quot;c&quot; is an abbreviation for &quot;capture&quot; or &quot;record&quot;</extracomment>
        <translatorcomment>Aufnahme</translatorcomment>
        <translation>a</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="58"/>
        <source>Split &amp;channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>&amp;Kanäle trennen</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="62"/>
        <source>Join &amp;channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>&amp;Kanäle verbinden</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="66"/>
        <source>&amp;Level channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>Kanäle &amp;nivellieren</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer_HCTL</name>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="44"/>
        <source>Element name</source>
        <translation>Öğe adı</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="48"/>
        <source>Joined</source>
        <translation>Birleştirildi</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="49"/>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="121"/>
        <source>Element index</source>
        <translation>Öğe sırası</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="50"/>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="53"/>
        <source>Channel %1</source>
        <translation>Kanal %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="52"/>
        <source>Ch. %1</source>
        <translation>Kan. %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="57"/>
        <source>Index: %1</source>
        <translation>Sıra: %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="58"/>
        <source>Channel: %2</source>
        <translation>Kanal: %2</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="114"/>
        <source>Index:</source>
        <translation>Sıra:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="115"/>
        <source>Device:</source>
        <translation>Aygıt:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="116"/>
        <source>Flags:</source>
        <translation>Etiketler:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="117"/>
        <source>Channels:</source>
        <translation>Kanallar:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="118"/>
        <source>Num. id:</source>
        <translation>Numara:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="122"/>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="138"/>
        <source>Device</source>
        <translation>Aygıt</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="123"/>
        <source>Flags</source>
        <translation>Etiketler</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="124"/>
        <source>Channel count</source>
        <translation>Kanal sayısı</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="125"/>
        <source>Numeric Id</source>
        <translation>Numerik Id</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="139"/>
        <source>Subdevice</source>
        <translation>Alt aygıt</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl.cpp" line="337"/>
        <source>No element selected</source>
        <translation>Seçili eleman yok</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer_HCTL_Edit_Int</name>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_edit_int.cpp" line="52"/>
        <source>minimum</source>
        <translation>Asgari</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_edit_int.cpp" line="52"/>
        <source>maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_edit_int.cpp" line="53"/>
        <source>Integer range:</source>
        <translation>Tamsayı aralığı:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_edit_int.cpp" line="54"/>
        <source>Decibel range:</source>
        <translation>Desibel aralığı:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_edit_int.cpp" line="143"/>
        <source>Channel %1</source>
        <translation>Kanal %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_edit_int.cpp" line="144"/>
        <source>Index %1</source>
        <translation>Sıra %1</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer_HCTL_Edit_Unsupported</name>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_edit_unsupported.cpp" line="35"/>
        <source>Elements of the type %1 are not supported</source>
        <translation>Elemente vom Typ %1 werden nicht unterstützt</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer_HCTL_Int_Proxy_Column</name>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_int_proxy_column.cpp" line="25"/>
        <source>%1 dB</source>
        <extracomment>Decibel value string template</extracomment>
        <translation>%1 dB</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_int_proxy_column.cpp" line="27"/>
        <source>%1 %</source>
        <extracomment>Percent value string template</extracomment>
        <translation>%1 %</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer_HCTL_Table_Model</name>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="35"/>
        <source>%1,%2</source>
        <translation>%1,%2</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="43"/>
        <source>Name</source>
        <translation>Ad</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="44"/>
        <source>Element name</source>
        <translation>Öğe adı</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="47"/>
        <source>Idx</source>
        <extracomment>Idx - abbreviation for Index</extracomment>
        <translation>Idx</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="48"/>
        <source>Element index</source>
        <translation>Öğe sırası</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="50"/>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="51"/>
        <source>Device</source>
        <translation>Aygıt</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="53"/>
        <source>Type</source>
        <translation>Tür</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="54"/>
        <source>Element type</source>
        <translation>Öğe türü</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="57"/>
        <source>Ch.</source>
        <extracomment>Ch. - abbreviation for Channel</extracomment>
        <translation>Kan.</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="58"/>
        <source>Channel count</source>
        <translation>Kanal sayısı</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="60"/>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="61"/>
        <source>Flags</source>
        <translation>Etiketler</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="63"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/mixer_hctl_table_model.cpp" line="64"/>
        <source>Numeric Id</source>
        <translation>Numerik Id</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer_Sliders</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders.cpp" line="81"/>
        <source>&amp;Mute</source>
        <translation>&amp;Sesi kıs</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders.cpp" line="82"/>
        <source>&amp;Mute all</source>
        <translation>&amp;Hepsini kıs</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders.cpp" line="83"/>
        <source>Un&amp;mute</source>
        <translation>Sesi &amp;aç</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders.cpp" line="84"/>
        <source>Un&amp;mute all</source>
        <translation>H&amp;epsini aç</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders.cpp" line="85"/>
        <source>Toggle &amp;mutes</source>
        <translation>Ses kımayı &amp;değiştir</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders.cpp" line="112"/>
        <source>Playback slider</source>
        <translation>Oynatım ayarı</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders.cpp" line="113"/>
        <source>Capture slider</source>
        <translation>Yakalama ayarı</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders.cpp" line="114"/>
        <source>Playback switch</source>
        <translation>Oynatım anahtarı</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders.cpp" line="115"/>
        <source>Capture switch</source>
        <translation>Yakalama anahtarı</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders.cpp" line="308"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer_Sliders_Proxies_Column</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders_proxies_column.cpp" line="27"/>
        <source>%1 dB</source>
        <translation>%1 dB</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_sliders_proxies_column.cpp" line="28"/>
        <source>%1 %</source>
        <translation>%1 %</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer_Switches</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_switches.cpp" line="289"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_switches.cpp" line="304"/>
        <source>Playback selection</source>
        <translation>Oynatım seçimi</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_switches.cpp" line="306"/>
        <source>Playback switch</source>
        <translation>Oynatım anahtarı</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_switches.cpp" line="310"/>
        <source>Capture selection</source>
        <translation>Yakalama seçimi</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer_switches.cpp" line="312"/>
        <source>Capture switch</source>
        <translation>Yakalama anahtarı</translation>
    </message>
</context>
<context>
    <name>MWdg::Slider_Status_Widget</name>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="37"/>
        <source>Slider value</source>
        <translation>Kaydırıcı değeri</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="46"/>
        <source>Value</source>
        <translation>Değer</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="49"/>
        <source>Max.</source>
        <translation>Maks.</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="50"/>
        <source>Maximum</source>
        <translation>Azami</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="53"/>
        <source>Min.</source>
        <translation>Min.</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="55"/>
        <source>Minimum</source>
        <translation>Asgari</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="61"/>
        <source>Volume</source>
        <translation>Ses</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="67"/>
        <source>Current volume</source>
        <translation>Şimdiki ses</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="73"/>
        <source>Volume maximum</source>
        <translation>Azami ses</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="78"/>
        <source>Volume minimum</source>
        <translation>Asgari ses</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="86"/>
        <source>Decibel</source>
        <translation>Desibel</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="92"/>
        <source>Current Decibel value</source>
        <translation>Şimdiki Desibel değeri</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="100"/>
        <source>Decibel maximum</source>
        <translation>Azami Desibel</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="105"/>
        <source>Decibel minimum</source>
        <translation>Asgari Desibel</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="111"/>
        <source>Element name</source>
        <translation>Öğe adı</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="351"/>
        <source>Close slider value display</source>
        <translation>Kaydırıcı değerini gösterme</translation>
    </message>
</context>
<context>
    <name>Main_Window</name>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="42"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="41"/>
        <source>&amp;Fullscreen mode</source>
        <translation>&amp;Tam ekran modu</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="43"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="42"/>
        <source>Exit &amp;fullscreen mode</source>
        <translation>&amp;Tam ekran modundan çık</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="129"/>
        <location filename="../../qashctl/src/main_window.cpp" line="153"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="63"/>
        <source>&amp;Quit</source>
        <translation>&amp;Çıkış</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/main_window.cpp" line="68"/>
        <source>&amp;Settings</source>
        <translation>&amp;Ayarlar</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/main_window.cpp" line="69"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+e</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="134"/>
        <location filename="../../qashctl/src/main_window.cpp" line="169"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="73"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Yenile</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="159"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="79"/>
        <source>Show &amp;device selection</source>
        <translation>Aygıt adı boş</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="139"/>
        <location filename="../../qashctl/src/main_window.cpp" line="174"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="88"/>
        <source>&amp;Info</source>
        <translation>&amp;Bilgi</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="145"/>
        <location filename="../../qashctl/src/main_window.cpp" line="180"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="95"/>
        <source>&amp;File</source>
        <translation>&amp;Dosya</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="151"/>
        <location filename="../../qashctl/src/main_window.cpp" line="186"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="101"/>
        <source>&amp;View</source>
        <translation>&amp;Görünüm</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="157"/>
        <location filename="../../qashctl/src/main_window.cpp" line="195"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="108"/>
        <source>&amp;Help</source>
        <translation>&amp;Yardım</translation>
    </message>
</context>
<context>
    <name>QSnd::Alsa_Config_Model</name>
    <message>
        <location filename="../../qasconfig/src/qsnd/alsa_config_model.cpp" line="255"/>
        <source>Key</source>
        <translation>Anahtar</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/qsnd/alsa_config_model.cpp" line="257"/>
        <source>Value</source>
        <translation>Değer</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_HCTL</name>
    <message>
        <location filename="../../shared/src/qsnd/mixer_hctl.cpp" line="110"/>
        <source>Empty device name</source>
        <translation>Aygıt adı boş</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_Simple</name>
    <message>
        <location filename="../../shared/src/qsnd/mixer_simple.cpp" line="124"/>
        <source>Empty device name</source>
        <translation>Boş aygıt ağı</translation>
    </message>
</context>
<context>
    <name>Tray_Mixer_Balloon</name>
    <message>
        <location filename="../../qasmixer/src/tray_mixer_balloon.cpp" line="34"/>
        <source>Volume is at %1%</source>
        <translation>Ses düzeyi %1%</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray_mixer_balloon.cpp" line="40"/>
        <source>Show mixer</source>
        <translation>Karıştırıcıyı göster</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray_mixer_balloon.cpp" line="67"/>
        <source>Volume muted</source>
        <translation>Sessiz</translation>
    </message>
</context>
<context>
    <name>Tray_Mixer_Icon</name>
    <message>
        <location filename="../../qasmixer/src/tray_mixer_icon.cpp" line="34"/>
        <source>&amp;Show mixer</source>
        <translation>&amp;Karıştırıcıyı göster</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray_mixer_icon.cpp" line="35"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+s</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray_mixer_icon.cpp" line="40"/>
        <source>&amp;Close %1</source>
        <extracomment>%1 will be replaced with the program title</extracomment>
        <translation>%1 Ka&amp;pat</translation>
    </message>
</context>
<context>
    <name>Views::Alsa_Config_View</name>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="37"/>
        <source>ALSA configuration</source>
        <translation>ALSA Ayarları</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="41"/>
        <source>&amp;Expand</source>
        <translation>&amp;Genişlet</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="42"/>
        <source>Co&amp;llapse</source>
        <translation>&amp;Daralt</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="43"/>
        <source>&amp;Sort</source>
        <translation>&amp;Sırala</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="46"/>
        <source>Depth:</source>
        <translation>Derinlik:</translation>
    </message>
</context>
<context>
    <name>Views::Basic_Dialog</name>
    <message>
        <location filename="../../shared/src/views/basic_dialog.cpp" line="93"/>
        <source>&amp;Close</source>
        <translation>&amp;Kapat</translation>
    </message>
</context>
<context>
    <name>Views::Device_Selection_View</name>
    <message>
        <location filename="../../shared/src/views/dev_select_view.cpp" line="51"/>
        <source>&amp;Close device selection</source>
        <translation>&amp;Aygıt seçimini kapat</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/dev_select_view.cpp" line="79"/>
        <source>Mixer device</source>
        <translation>Karıştırıcı aygıt</translation>
    </message>
</context>
<context>
    <name>Views::Info_Dialog</name>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="38"/>
        <source>Info</source>
        <translation>Bilgi</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="60"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="72"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="75"/>
        <source>Home page</source>
        <translation>Anasayfa</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="79"/>
        <source>Project page</source>
        <translation>Proje sayfası</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="109"/>
        <source>Developers</source>
        <translation>Geliştiriciler</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="118"/>
        <source>Translators</source>
        <translation>Çevirmenler</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="175"/>
        <source>Information</source>
        <translation>Bilgi</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="176"/>
        <source>People</source>
        <translation>Yazanlar</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="177"/>
        <source>License</source>
        <translation>Lisans</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="64"/>
        <source>%1 is a collection of desktop applications for the Linux sound system %2.</source>
        <translation>%1, %2 Linux ses sistemi için masaüstü uygulamaları koleksiyonudur.</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="113"/>
        <source>Contributors</source>
        <translation>Katkı sunanlar</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="154"/>
        <source>The license file %1 is not available.</source>
        <translation>%1 lisans dosyası mevcut değil.</translation>
    </message>
</context>
<context>
    <name>Views::Message_Widget</name>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="42"/>
        <source>Mixer device couldn&apos;t be opened</source>
        <translation>Karıştırıcı aygıt açılamadı</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="43"/>
        <source>No device selected</source>
        <translation>Seçili aygıt yok</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="83"/>
        <source>Function</source>
        <translation>Görev</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="84"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="85"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
</context>
<context>
    <name>Views::Mixer_Simple</name>
    <message>
        <location filename="../../qasmixer/src/views/mixer_simple.cpp" line="52"/>
        <source>Show playback</source>
        <translation>Oynatmayı göster</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/mixer_simple.cpp" line="53"/>
        <source>Show capture</source>
        <translation>Yakalamayı göster</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/mixer_simple.cpp" line="54"/>
        <source>Show playback elements</source>
        <translation>Kayıttan yürütme öğelerini göster</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/mixer_simple.cpp" line="55"/>
        <source>Show capture elements</source>
        <translation>Yakalama öğelerini göster</translation>
    </message>
</context>
<context>
    <name>Views::Settings_Dialog</name>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="41"/>
        <source>Settings</source>
        <translation>Ayarlar</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="124"/>
        <source>Startup</source>
        <translation>Başlangıç</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="132"/>
        <source>Sliders</source>
        <translation>Kaydırıcılar</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="156"/>
        <source>Appearance</source>
        <translation>Görünüm</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="215"/>
        <source>Input</source>
        <translation>Giriş</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="349"/>
        <source>System tray</source>
        <translation>Sistem tepsisi</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="71"/>
        <source>Startup mixer device</source>
        <translation>Karıştırıcı aygıtı başlat</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="76"/>
        <source>From last session</source>
        <translation>Son oturumdan</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="135"/>
        <source>Show slider value labels</source>
        <translation>Kaydırıcı değerini göster</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="164"/>
        <source>Mouse wheel</source>
        <translation>Fare tekeri</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="167"/>
        <source>Number of turns for a slider change from 0% to 100%</source>
        <translation>Kaydırıcı için %0'dan %100'e dönüş sayısı</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="222"/>
        <source>Show tray icon</source>
        <translation>Tepsi simgesi göster</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="223"/>
        <source>Close to tray</source>
        <translation>Sistem tepsisine kapatın</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="233"/>
        <source>System tray usage</source>
        <translation>Sistem tepsisi kullanımı</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="243"/>
        <source>Notification balloon</source>
        <translation>Bildirim kutusu</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="246"/>
        <source>Show balloon on a volume change</source>
        <translation>Ses değişikliğinde balon ileti göster</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="254"/>
        <source>Balloon lifetime</source>
        <translation>Balon ileti süresi</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="257"/>
        <source>ms</source>
        <extracomment>ms - milisaniye kısaltması</extracomment>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="289"/>
        <source>Mini mixer device</source>
        <translation>Mini karıştırıcı aygıt</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="75"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="291"/>
        <source>Default</source>
        <translation>Varsayılan</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="292"/>
        <source>Current (same as in main mixer window)</source>
        <translation>Şimdiki(ana karıştırıcı penceresindekiyle aynı)</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="77"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="293"/>
        <source>User defined</source>
        <translation>Kullanıcı tanımlı</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="87"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="303"/>
        <source>User device:</source>
        <translation>Kullanıcı aygıtı:</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="88"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="304"/>
        <source>e.g. hw:0</source>
        <translation>Örn. hw:0</translation>
    </message>
</context>
</TS>
